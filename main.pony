use "collections"

actor Main
  new create(env: Env) =>
    env.out.print("Hello, world!")
    let z = Wombat(env)
    z.test()
    var a = Aardvark("bla")
    for i in Range[U64](0, 100) do
      env.out.print(i.string())
      a.eat(i)
    end


class Wombat
  let name: String
  var _hunger_level: U64
  var _thirst_level: U64 = 1
  let _env: Env

  new create(env: Env) =>
    _env = env
    name = "something"
    _hunger_level = 5

  fun test() =>
    _env.out.print("Hello, world2!")



actor Aardvark
  let name: String
  var _hunger_level: I64 = 100
  let _env: Env

  new create(env': Env, name': String) =>
    _env = env'
    name = name'

  be eat(amount: U64) =>
    _hunger_level = _hunger_level - amount.i64()

  be hello() =>
    _env.out.print("Hello, world2!")

